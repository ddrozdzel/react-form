import { applyMiddleware, createStore } from 'redux';
import promiseMiddleware from 'redux-promise-middleware';
import reducer from "./app/reducers";
import thunkMiddleware from 'redux-thunk';
import logger from 'redux-logger'

export default createStore(reducer,  applyMiddleware(
    thunkMiddleware,
    promiseMiddleware(),
    logger
));