import React, { Component } from 'react';
import { connect } from "react-redux";
import {fetchUsers, addUser, deleteUser, updateUser} from "./app/actions/usersActions";
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import faSpinner from '@fortawesome/fontawesome-free-solid/faSpinner'
import AddUser from "./app/components/AddUser";
import UserList from "./app/components/UserList";
import Header from "./app/components/UI/header";
import Footer from "./app/components/UI/footer";
import './App.css';
import {
    BrowserRouter as Router,
    Route,
} from 'react-router-dom'
import EditUser from "./app/components/EditUser";
import mapStateToProps from './map-to-state';

class App extends Component {

    constructor() {
        super();
        this.addNewUser = this.addNewUser.bind(this);
        this.editUser = this.editUser.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
    }

    componentWillMount() {
        this.props.dispatch(fetchUsers());
    }

    addNewUser(user) {
        this.props.dispatch(addUser(user));
    }

    editUser(user) {
        this.props.dispatch(updateUser(user));
    }

    deleteUser(id) {
        this.props.dispatch(deleteUser(id));
    }


    render() {
        const { users, fetching } = this.props.users;
        if(fetching) {
            return <div><FontAwesomeIcon icon={faSpinner} spin /></div>
        }

        return (
            <div>
                <Router>
                    <div>
                        <Header />
                        <Route exact path="/" render={()=><AddUser addUser={this.addNewUser}/>}/>
                        <Route path="/nowy" render={()=><AddUser addUser={this.addNewUser}/>}/>
                        <Route path="/edytuj/:id" render={()=><EditUser editUser={this.editUser} fetchUser={this.props}/>}/>
                        <Route path="/lista"
                               render={()=>
                                   <UserList users={users} deleteUser={this.deleteUser} />
                               }
                        />
                    </div>


                </Router>
                <Footer />
             </div>
        )
    }
}

App = connect(mapStateToProps)(App);
export default App;
