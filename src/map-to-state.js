const mapStateToProps = (state) => {
    return {
        users: state.users,
    };
};

export default mapStateToProps;