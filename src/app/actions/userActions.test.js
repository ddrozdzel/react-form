import {fetchUser, fetchUsers, addUser, updateUser, deleteUser} from './usersActions';
import usersMockData from './users-mock-data';
import thunk from 'redux-thunk'
import configureStore from 'redux-mock-store'

const middlewares = [ thunk ];
const mockStore = configureStore(middlewares);

describe('other userActions tests', () => {
    it('should test fetchUsers action and return values', () => {

        const expectedActions = [
            { type: "FETCH_USERS" },
            { type: 'FETCH_USERS_FULLFILLED', data: { data: usersMockData } }
        ];
        const store = mockStore({});
        return store.dispatch(fetchUsers()).then(() => {
            expect(store.getActions()).toEqual(expectedActions)
        })
    });


    it('should test fetchUser action', () => {
        expect(fetchUser('test')).toEqual( {
            type: "FETCH_USER",
            data: 'test'
        })
    });

   it('should test addUser action', () => {
      expect(addUser('test')).toEqual( {
          type: "ADD_USER",
          data: 'test'
      })
   });

    it('should test updateUser action', () => {
        expect(updateUser('test')).toEqual( {
            type: "UPDATE_USER",
            data: 'test'
        })
    });

    it('should test deleteUser action', () => {
        expect(deleteUser(1)).toEqual( {
            type: "DELETE_USER",
            data: 1
        })
    });

});