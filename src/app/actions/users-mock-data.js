const usersMockData = [
    {
        id: 1,
        name: "Daniel",
        surName: "Drozdzel",
        email: "daniel.drozdzel@gmail.com",
        date: "27.02.2018",
        sex: 'M',
        country: 'Polska',
    },
    {
        id: 2,
        name: "Daniel2",
        surName: "Drozdzel2",
        email: "daniel.drozdzel+1@gmail.com",
        date: "27.02.2018",
        sex: 'M',
        country: 'Polska',
    },
    {
        id: 3,
        name: "Daniel3",
        surName: "Drozdzel3",
        email: "daniel.drozdzel+2@gmail.com",
        date: "27.02.2018",
        sex: 'M',
        country: 'Polska',
    }
];

export default usersMockData;