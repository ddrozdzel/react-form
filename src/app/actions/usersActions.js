import axios from "axios";
import MockAdapter from 'axios-mock-adapter';
import usersMockData from './users-mock-data'

//AXIOS MOCK
const mock = new MockAdapter(axios);
mock.onGet('/users').reply(200, {
    data: usersMockData
});

export function fetchUsers() {
    return function(dispatch) {
        dispatch({type: "FETCH_USERS"});
        axios.get("/users")
            .then((response)=>{
                dispatch({type: "FETCH_USERS_FULLFILLED", data: response.data.data})
            })
            .catch((error)=> {
                dispatch({type: "FETCH_USERS_REJECTED", data: error})
            })
    }
}

export function fetchUser(user) {
    return {
        type: "FETCH_USER",
        data: user
    }
}

export function addUser(user) {
    return {
        type: "ADD_USER",
        data: user
    }
}

export function updateUser(user) {
    return {
        type: "UPDATE_USER",
        data: user
    }
}

export function deleteUser(id) {
    return {
        type: "DELETE_USER",
        data: id
    }
}