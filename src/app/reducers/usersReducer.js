import users from "../models/users";

export default function reducer( state=users, action) {
    switch(action.type) {
        case "FETCH_USERS":
            return {...state, fetching: true};
        case "FETCH_USERS_REJECTED":
            return {...state, fetching: false, error: action.data};
        case "FETCH_USERS_FULLFILLED":
            return {...state, fetching: false, fetched: true, users: action.data };
        case "FETCH_USER":
            return {...state};

        case "ADD_USER":
            const newUsers = [...state.users];
            newUsers.push(action.data);
            return {...state, users: newUsers};
        case "UPDATE_USER": {
            const newUsers = [...state.users];
            const userIndex = newUsers.findIndex(user => user.id === action.data.id);
            newUsers[userIndex] = action.data;
            return {...state, users: newUsers}
        }
        case "DELETE_USER":
            return {...state, users: state.users.filter(user => user.id !== action.data)}
    }
    return state;
}