import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import PropTypes from 'prop-types';


export default class FormComponent extends Component {
    static propTypes = {
        handleChange: PropTypes.func.isRequired,
        handleSubmit: PropTypes.func.isRequired,
        userInfo: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);
        this.setAvailableCountries();
    }
    

    setAvailableCountries() {
        this.countries = [
            "Polska", "Czechy", "Słowacja", "Niemcy"
        ];
    }


    render() {
        const availCountries = this.countries.map(country =>  <option key={country} value={country}>{country}</option>);
        console.log('form', this.props);
        return (
            <Form onSubmit={this.props.handleSubmit}>
                <FormGroup>
                    <Label>
                        Imię
                        <Input name="name" type="text" onChange={this.props.handleChange}
                               value={this.props.userInfo.name}
                        />
                    </Label>
                </FormGroup>
                <FormGroup>
                    <Label>
                        Nazwisko
                        <Input name="surName" type="text" onChange={this.props.handleChange}
                               value={this.props.userInfo.surName}/>
                    </Label>
                </FormGroup>
                <FormGroup tag="fieldset">
                    <legend>Płeć</legend>
                    <FormGroup check>
                        <Label check>
                            <Input name="sex" value="M" type="radio" checked={this.props.userInfo.sex === 'M'}
                                   onChange={this.props.handleChange} /> M
                        </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                            <Input name="sex" value="F" type="radio" checked={this.props.userInfo.sex === 'F'}
                                   onChange={this.props.handleChange} /> K
                        </Label>
                    </FormGroup>
                </FormGroup>
                <FormGroup>
                    <Label>
                        Kraj
                        <Input type="select" name="country" value={this.props.userInfo.country} onChange={this.props.handleChange}>
                            {availCountries}
                        </Input>
                    </Label>
                </FormGroup>
                <FormGroup>
                    <Label>
                        Email
                        <Input name="email" type="email" value={this.props.userInfo.email}
                               onChange={this.props.handleChange} />
                    </Label>
                </FormGroup>
                <Button type="submit" >Zapisz</Button>
            </Form>
        )
    }

}