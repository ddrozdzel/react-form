import React, {Component} from 'react';
import { withRouter } from 'react-router-dom'
import NavItemLinks from './NavItemLinks';
import Crumps from "./crumps";
import Navigation from "./navigation";

class Header extends Component {
    constructor() {
        super();
        this.state = {
            crumps: "nowy formularz"
        };

    }

    componentWillMount() {
        this.navLinks = [
            {linkTo: '/nowy', crumps: 'nowy formularz', title: "Nowy Formularz"},
            {linkTo: '/lista', crumps: 'lista', title: "Lista"},
        ];
        this.setBeginingProps()
    }

    componentWillReceiveProps(nextProps) {
        const path = nextProps.location.pathname;
        this.pathCrumpFinder(path);
    }

    setBeginingProps() {
        const path = this.props.location.pathname;
        this.pathCrumpFinder(path);
    }

    pathCrumpFinder(path) {
        if (path !== "/") {
            const index = this.navLinks.findIndex((nav) => {
                return nav.linkTo === path;
            });
            if(index > -1) {
                this.setCrumps(this.navLinks[index].crumps);
            } else {
                this.setCrumps(path.slice(1));
            }

        }
    }

    setCrumps(crump) {
        this.setState({
            crumps: crump
        });
    }

    render() {
        const navItemLinks = this.navLinks.map((nav) => {
            return <NavItemLinks
                key={nav.linkTo}
                navInfo ={nav}
                active={this.state.crumps}
                />
        });

        return (
            <div className="row">
                <Navigation navItemLinks={navItemLinks}/>
                <Crumps crumps={this.state.crumps}/>
            </div>
        )
    }
}

export default withRouter(Header);