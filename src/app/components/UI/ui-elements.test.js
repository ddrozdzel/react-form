import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import TestRenderer from 'react-test-renderer';
import ReactTestUtils from 'react-dom/test-utils';
import Footer from './footer';
import NavItemLinks from './NavItemLinks';
import { StaticRouter } from 'react-router'
import Header from './header';
import Crumps from './crumps';
import {
    Breadcrumb,
    BreadcrumbItem,
    NavbarBrand,
    Nav,
} from 'reactstrap';
import Navigation from './navigation';


describe('UI elements tests', () => {
    let shadowRenderer;
    beforeEach(() => {
        shadowRenderer = new ShallowRenderer();
    });

    it('should check Footer', () => {
        shadowRenderer.render(<Footer />);
        const result = shadowRenderer.getRenderOutput();
        expect(result).toEqual(<footer>CopyRight By Daniel</footer>);
    });

    it('should check crumps', () => {
        shadowRenderer.render(<Crumps crumps='test' />);
        const result = shadowRenderer.getRenderOutput();
        expect(result).toEqual(
            <div className="col-md-12">
                <Breadcrumb>
                    <BreadcrumbItem> Formularz testowy </BreadcrumbItem>
                    <BreadcrumbItem active>test</BreadcrumbItem>
                </Breadcrumb>
            </div>);
    });

    it('should check Navigation', () => {
        const navItemLinksDummy = 'dummy';
        const navItemLink = TestRenderer.create(
            <Navigation navItemLinks={navItemLinksDummy}/>
        );
        const instance = navItemLink.root;
        expect(instance.findByType(NavbarBrand).props.children).toEqual('Formularz testowy');
        expect(instance.findByType(Nav).props.children).toEqual('dummy');

    });

    it('should check NavItemLinks with active crumps', () => {
        const active = '/test';
        const navInfo = {
            linkTo: '/link',
            crumps: '/test',
            title: 'title'
        };
        const navItemLink = TestRenderer.create(
            <StaticRouter location="someLocation" context='context'>
                <NavItemLinks navInfo={navInfo} active={active}/>
            </StaticRouter>).toJSON();
        expect(navItemLink.children[0].props.href).toEqual('/link');
        expect(navItemLink.children[0].children[0]).toEqual(
            {
                "children": ["title"],
                "props": {
                "className": "pointer nav-link active"
            },
                "type": "span"
            }
        )
    });

    it('should check NavItemLinks with not-active crumps', () => {
        const active = '/test';
        const navInfo = {
            linkTo: '/link',
            crumps: '/test/2',
            title: 'title'
        };
        const navItemLink = TestRenderer.create(
            <StaticRouter location="someLocation" context='context'>
                <NavItemLinks navInfo={navInfo} active={active}/>
            </StaticRouter>).toJSON();
        expect(navItemLink.children[0].props.href).toEqual('/link');
        expect(navItemLink.children[0].children[0]).toEqual(
            {
                "children": ["title"],
                "props": {
                    "className": "pointer nav-link null"
                },
                "type": "span"
            }
        )
    });
});


// describe('header element test', () => {
//     let header;
//     beforeEach(() => {
//         header = TestRenderer.create(<Header />);
//     });
//
//     it('should check initial values', () => {
//
//     });
//
// });