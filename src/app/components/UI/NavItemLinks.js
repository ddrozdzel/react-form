import React from 'react';
import { NavItem } from 'reactstrap';
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';

const NavItemLinks = ({navInfo, active}) => (
    <NavItem>
        <Link to={navInfo.linkTo}>
                <span className={"pointer nav-link "+ (active === navInfo.crumps ? 'active' : null)}>
                    {navInfo.title}
                </span>
        </Link>
    </NavItem>
);


NavItemLinks.propTypes = {
    navInfo: PropTypes.object.isRequired ,
    active: PropTypes.string.isRequired ,
};

export default NavItemLinks;