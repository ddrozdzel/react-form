import React from 'react';
import {
    Collapse,
    Navbar,
    NavbarBrand,
    Nav,
} from 'reactstrap';
import PropTypes from 'prop-types';

const Navigation = ({navItemLinks}) => (
    <div className="col-md-12">
        <Navbar color="light" light expand="md">
            <NavbarBrand href="/" className="mr-auto">Formularz testowy</NavbarBrand>
            <Collapse isOpen={true} navbar>
                <Nav className="ml-auto" tabs>
                    {navItemLinks}
                </Nav>
            </Collapse>
        </Navbar>
    </div>
);


Navigation.propTypes = {
    navItemLinks: PropTypes.object.isRequired ,
};

export default Navigation;