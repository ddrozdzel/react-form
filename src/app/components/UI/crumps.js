import React from 'react';
import {
    Breadcrumb,
    BreadcrumbItem,
} from 'reactstrap';
import PropTypes from 'prop-types';


const Crumps = ({crumps}) => (
    <div className="col-md-12">
        <Breadcrumb>
            <BreadcrumbItem> Formularz testowy </BreadcrumbItem>
            <BreadcrumbItem active>{crumps}</BreadcrumbItem>
        </Breadcrumb>
    </div>
);

Crumps.propTypes = {
    crumps: PropTypes.string.isRequired ,
};

export default Crumps;