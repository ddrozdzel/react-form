import React, { Component } from 'react';
import { Table } from 'reactstrap';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faEdit from '@fortawesome/fontawesome-free-solid/faEdit';
import faTrash from '@fortawesome/fontawesome-free-solid/faTrash';
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';

export default class UserList extends Component{
    static propTypes = {
        deleteUser: PropTypes.func.isRequired,
        users: PropTypes.array.isRequired
    };


    editUser(user) {
        this.props.editUser(user);
    }

    deleteUser(id) {
        this.props.deleteUser(id);
    }


    render() {
        const users = this.props.users;
        const mappedUsers = users.map((user) => {
            return  <tr key={user.id}>
                <th scope="row">{user.id}</th>
                <td>{user.name}</td>
                <td>{user.surName}</td>
                <td>{user.sex}</td>
                <td>{user.country}</td>
                <td>{user.email}</td>
                <td>
                    <Link to={`/edytuj/${user.id}`}>
                        <span className="float-left pointer" >
                            <FontAwesomeIcon icon={faEdit}/>
                        </span>
                    </Link>
                    <span className="float-right pointer" onClick={this.deleteUser.bind(this, user.id)}>
                        <FontAwesomeIcon  icon={faTrash} />
                    </span>
                </td>
            </tr>
        });

        return (
            <Table>
                <thead>
                <tr>
                    <th>#</th>
                    <th>Imie</th>
                    <th>Nazwisko</th>
                    <th>Płeć</th>
                    <th>Kraj</th>
                    <th>Email</th>
                    <th>Akcje</th>
                </tr>
                </thead>
                <tbody>
                    {mappedUsers}
                </tbody>
            </Table>
        )
    }

}
