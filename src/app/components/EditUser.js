import React, { Component } from 'react';
import  FormComponent from './Form';
import { withRouter } from 'react-router-dom'
import {connect} from "react-redux";
import mapStateToProps from '../../map-to-state';
import PropTypes from 'prop-types';

class EditUser extends Component {
    static propTypes = {
        editUser: PropTypes.func.isRequired,
        fetchUser: PropTypes.object.isRequired
    };

    constructor() {
        super();
        this.initState();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }


    initState() {
        this.state = {
            id: Date.now(),
            name: '',
            surName: '',
            sex: 'M',
            country: 'Polska',
            email: '',
            date: ''
        };
    }

    componentDidMount() {
        const paramId = this.props.match.params.id;
        if (paramId) {
            this.setUserFetchedData(paramId);

        }
    }

    setUserFetchedData(paramId) {
        const user = this.props.users.users[paramId - 1];
        this.setState({
            ...user
        })
    }


    handleSubmit(event) {
        this.props.editUser(this.state);
        event.preventDefault();
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    render() {
        return (
            <div>
                <FormComponent handleChange={this.handleChange} handleSubmit={this.handleSubmit} userInfo={this.state} />
            </div>

        )
    }

}

EditUser = connect(mapStateToProps)(EditUser);
export default withRouter(EditUser);