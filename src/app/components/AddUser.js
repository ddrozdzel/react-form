import React, { Component } from 'react';
import  FormComponent from './Form';
import PropTypes from 'prop-types';

export default class AddUser extends Component {
    static propTypes = {
        addUser: PropTypes.func.isRequired
    };

    constructor() {
        super();
        this.initState();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }


    initState() {
        this.state = {
            id: Date.now(),
            name: '',
            surName: '',
            sex: 'M',
            country: 'Polska',
            email: '',
            date: ''
        };
    }

    handleSubmit(event) {
        this.props.addUser(this.state);
        event.preventDefault();
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    render() {
        return (
            <FormComponent handleChange={this.handleChange} handleSubmit={this.handleSubmit} userInfo={this.state} />
        )
    }

}
